import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {DEFAULT_BOOKS_IMAGE} from './Constraints.js';
import './App.css';

const useStyles = makeStyles({
  root: {
    minWidth: 400,
    alignContent: 'center',
    minHeight: 300,
    backgroundColor: '#F8F8F8'
  },
  title: {
    margin: 15
  },
  button: {
    margin: 25,
    backgroundColor: "#8ba8d3",
    color: "white",
    width: 150,
    height: 50
  },
  textField:{
    marginBottom: 20,
    width:300
  },
  mainGrid:{
    minHeight: '100vh',
    backgroundColor: '#E8E8E8',
    width:'100%'
  },
  logo:{
    height:300, 
    width:300
  }
});


function App() {

  const [email,setEmail] = useState("");
  const [password,setPassword] = useState("");
  const [loginError,setLoginError] = useState(false);

  let history = useHistory();

  const classes = useStyles();

  function login(event) {
    event.preventDefault();
    fetch("http://localhost:8080/login", {
      method: "POST",
      mode: "cors",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ username: email, password: password })
    })
      .then(response => {
        if(response.ok){
          localStorage.setItem("token", response.headers.get("Authorization"));
          history.push("/books");
        }else{
          setLoginError(true)
        }
      })
  }

  function setProperty(propertyName, value){
    if(propertyName === 'email'){
      setEmail(value)
    } else if(propertyName === 'password'){
      setPassword(value)
    }
  }
  function areLoginParamsEmpty(){
    return email == "" || password == ""
  }

  return (
    <Grid
    container
    alignItems="center"
    justify="center"
    className={classes.mainGrid}
   >
          <Card raised className={classes.root}>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <Typography className={classes.title} variant="h3" >
                Library
              </Typography>
              <img src = {DEFAULT_BOOKS_IMAGE} className={classes.logo}></img>
              <TextField className={classes.textField} label="Email" variant="outlined" value={email} onChange={(e) => setProperty('email',e.target.value)}></TextField>
              <TextField className={classes.textField} type="password" label="Password" variant="outlined" value={password} onChange={(e) => setProperty('password',e.target.value)}></TextField>
              {loginError && <Typography className={classes.title} color="error">
                Wrong email or password
              </Typography>}
              <Button disabled = {areLoginParamsEmpty()} className={classes.button} onClick={login} variant="contained">
                Login
              </Button>
            </Grid>
          </Card>
     </Grid>
    
  );
}

export default App;
