import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import App from './App'
import BookList from "./components/BookList";
import BookDetails from "./components/BookDetails";
import NavBar from "./components/NavBar"
import Profile from "./components/Profile"
import BookForm from "./components/BookForm"

const createRoutes = () => (
    <BrowserRouter>
        <Route path="/" component={ ( props ) => ( props.location.pathname !== "/") && <NavBar /> }></Route>
        <Route exact path="/" component={App}></Route>
        <Route exact path="/books" component={BookList}></Route>
        <Route exact path="/profile" component={Profile}></Route>
        <Route exact path="/newBook" component={BookForm}></Route>
        <Route exact path="/books/:id" component={BookDetails}></Route>


    </BrowserRouter>
)

export default createRoutes;