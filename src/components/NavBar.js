import React, { useEffect,useState } from 'react';
import { useHistory } from "react-router-dom";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Avatar from '@material-ui/core/Avatar';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import AddBoxIcon from '@material-ui/icons/AddBox';
import { makeStyles } from '@material-ui/core/styles';
import AccountCircle from '@material-ui/icons/AccountCircle';
import {DEFAULT_BOOKS_IMAGE_NAVBAR, ACCOUNT_MENU_ID} from '../Constraints.js';

function NavBar() {

    const useStyles = makeStyles({
        bar: {
            background: "#8ba8d3"
        },
        title: {
            cursor: "pointer"
        },
        button: {
            alignContent: "right",
            marginLeft:'auto'
        },
        headerElement:{
            marginRight: '10px',
            color:'white'
        },
        booksButton:{
            color: 'white'
        }
    });

    const classes = useStyles();

    let history = useHistory();

    const [anchorEl, setAnchorEl] = React.useState(null);

    const [user, setUser] = useState("");

    const isMenuOpen = Boolean(anchorEl);

    const handleMenuClose = (target) => {
        if(target === "profile"){
            history.push('/profile')
        }
        if(target === "logout"){
            window.localStorage.setItem('token',"");
            history.push('/')

        }
        setAnchorEl(null);
    };

    const handleProfileMenuOpen = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const renderMenu = (
        <Menu
            anchorEl={anchorEl}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            id={ACCOUNT_MENU_ID}
            keepMounted
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={isMenuOpen}
            onClose={handleMenuClose}
        >
            <MenuItem onClick={() => handleMenuClose("profile")}>{user.name} {user.surname}</MenuItem>
            <MenuItem onClick={() => handleMenuClose("logout")}>Logout</MenuItem>
        </Menu>
    );

    useEffect(()=>{
        fetch('http://localhost:8080/users/current',{
            headers:{
              "Authorization": localStorage.getItem("token")
            }
          })
          .then(response =>{
            if(response.ok){
                response.json()
                .then(data => {
                    setUser(data);
                });
            }else{
                history.push('/')
            }
          })
    }, [])

    return (
        <div>
            <AppBar position="static" className={classes.bar}>
                <Toolbar>
                    <Avatar className={classes.headerElement} alt="Library" src={DEFAULT_BOOKS_IMAGE_NAVBAR} />
                    <Button className={classes.headerElement} onClick={()=>history.push('/books')} >
                        <LibraryBooksIcon />
                        <Typography variant="h6" color="inherit">
                        Books
                        </Typography>
                    </Button> 
                    <Button className={classes.headerElement} onClick={()=>history.push('/newBook')} style={{color:'white'}} >
                        <AddBoxIcon />
                        <Typography variant="h6" color="inherit">
                        ADD BOOK
                        </Typography>
                    </Button> 
                    <IconButton className={classes.button}
                        edge="end"
                        aria-label="account of current user"
                        aria-controls={ACCOUNT_MENU_ID}
                        aria-haspopup="true"
                        onClick={handleProfileMenuOpen}
                        color="inherit"
                    >
                        <AccountCircle />

                    </IconButton>
                </Toolbar>
            </AppBar>
            {renderMenu}
        </div>
    )
}

export default NavBar;