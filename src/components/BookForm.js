import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';
import React, { useEffect,useState } from 'react';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import CreateIcon from '@material-ui/icons/Create';
import { Button, Typography } from '@material-ui/core';

import { FormControl } from '@material-ui/core';
import { InputLabel } from '@material-ui/core';
import { Select,MenuItem,ListItem,ListItemAvatar,Avatar,ListItemText,List,IconButton } from '@material-ui/core';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import AuthorList from './AuthorList'

export default function BookForm(){


    const [imageUrl,setImageUrl] = useState("https://breakthrough.org/wp-content/uploads/2018/10/default-placeholder-image-300x300.png");
    const [textFiledImg,setTextFiledImg] = useState("");
    const [title,setTitle] = useState("");
    const [description,setDescription] = useState("");
    const [authors,setAuthors] = useState([]);
    const [selectedAuthors,setSelectedAuthros] = useState([]);

    const [firstName,setFirstName] = useState("");
    const [middleName,setMiddleName] = useState("");
    const [lastName,setLastName] = useState("");


    const useStyles = makeStyles((theme) => ({
        card:{
            maxWidth: '800px',
            marginLeft:'auto',
            marginRight: 'auto',
            marginTop: '50px',
            minHeight: '700px',
            padding: '50px',
            alignItems:'center'
        },
        decription:{
            minWidth: '100%',
            marginBottom: '30px'

        },
        title:{
           minWidth: '100%',
           marginBottom: '33px'
        },
        rightDiv:{
            float: 'left',
            maxWidth: '50%',
            margin:'20px'
        },
        leftDiv:{
            float: 'left',
            maxWidth: '50%',
            margin:'20px'
        },
        img:{
            display:'block',
            margin:'auto',
            maxWidth:'300px',
            maxHeight:'300px',
        },
        imageInput:{
            width:'300px',
            marginTop:'20px',
        },
        select:{
            minWidth: '100%'
        },
        listAvatar:{
            width:'30px',
            height:'30px'
        },
        addAuthorTypography:{
            marginBottom:'30px',
            marginTop:'30px'
        },
        addBookButton:{
            marginTop: '25px',
            marginLeft:'40%'
        }


    }));

    const classes = useStyles();

    function resetImg(){
        setImageUrl('https://breakthrough.org/wp-content/uploads/2018/10/default-placeholder-image-300x300.png');
    }

    function setImg(link){
        setTextFiledImg(link);
        setImageUrl(link);
    }

    function removeSelectedAuthor(author){        
        selectedAuthors.splice(selectedAuthors.indexOf(author),1);
        setSelectedAuthros(selectedAuthors);
        
        setAuthors([...authors,author])
        

    }
    
    function addNewAuthor(){
        let author = {};
        author.firstName= firstName;
        author.middleName = middleName;
        author.lastName = lastName;
        setSelectedAuthros([...selectedAuthors,author]);
    }

    const handleChangeSelect = (event) => {
        let author = event.target.value;
        setSelectedAuthros(author);
        authors.splice(authors.indexOf(author[selectedAuthors.length]), 1);
      };

     function setProperty(propertyName, value){
        if(propertyName === 'firstName'){
          setFirstName(value)
        } else if(propertyName === 'middleName'){
          setMiddleName(value)
        } else if(propertyName === 'lastName'){
            setLastName(value)
        }
      }
      
      function submitNewBook(){
            fetch("http://localhost:8080/books", {
                method: "POST",
                mode: "cors",
                credentials: "same-origin",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": localStorage.getItem("token")
                },
                body: JSON.stringify({
                    name: title, description: description, authors: selectedAuthors,
                    imageUrl: imageUrl
                })
            })
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                })
      }

    useEffect(()=>{

        fetch('http://localhost:8080/authors',{
          headers:{
            "Authorization": localStorage.getItem("token")
          }
        })
        .then(response => response.json())
        .then(data => {
            setAuthors(data);
        });
    }, []);


    return(
        <Card raised className={classes.card}>
            <div className={classes.leftDiv}>
                 <img onError={resetImg} className={classes.img} src={imageUrl}/>
                 <TextField
                    id="outlined-required"
                    label="Image url"
                    variant="outlined"
                    alt="https://breakthrough.org/wp-content/uploads/2018/10/default-placeholder-image-300x300.png"
                    value={textFiledImg}
                    className={classes.imageInput}
                    onChange={(e) => setImg(e.target.value)}
                />
                <AuthorList authors={selectedAuthors} listType="complexList" removeSelectedAuthor={removeSelectedAuthor}/>
                 </div>
            <div className={classes.rightDiv}>
                <TextField
                    className={classes.title}
                    id="outlined-required"
                    label="Title"
                    value = {title}
                    onChange={(e) => setTitle(e.target.value)}
                />
                <TextField
                    className={classes.decription}
                    id="outlined-multiline-static"
                    label="Description"
                    multiline
                    rows={9}
                    variant="outlined"
                    value = {description}
                    onChange={(e) => setDescription(e.target.value)}
                />
                 <FormControl className={classes.select} variant="outlined">
                    <InputLabel>Select authors</InputLabel>

                    <Select
                        multiple
                        value={selectedAuthors}
                        onChange={handleChangeSelect}
                        variant="outlined"
                        label="Select authors"

                    >          
                        {authors.map((author,index) => (
                            <MenuItem key={index} value={author}>
                            {author.firstName}
                            </MenuItem>
                        ))}


                    </Select>
                    <Typography className={classes.addAuthorTypography} variant="h6" >Add new Author</Typography>
                    <TextField
                        className={classes.title}
                        id="outlined-required"
                        label="First Name"
                        onChange={(e) => setProperty("firstName",e.target.value)}
                        variant="outlined"
                   />
                    <TextField
                        className={classes.title}
                        id="outlined-required"
                        label="Middle Name"
                        onChange={(e) => setProperty("middleName",e.target.value)}
                        variant="outlined"
                    />
                    <TextField
                        className={classes.title}
                        id="outlined-required"
                        label="Last Name"
                        onChange={(e) => setProperty("lastName",e.target.value)}
                        variant="outlined"
                    />
                    </FormControl>
                    <Button onClick={addNewAuthor} variant="contained" color = "primary">Add Author</Button>
            </div>
            <Button onClick={submitNewBook} className={classes.addBookButton}  size="large" variant="contained" color="primary">Add book</Button>
        </Card>
    );
}