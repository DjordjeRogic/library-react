
import React, { useEffect,useState } from 'react';
import Card from '@material-ui/core/Card';
import Avatar from '@material-ui/core/Avatar';
import { Typography } from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { makeStyles } from '@material-ui/core/styles';


export default function NavBar() {
    const useStyles = makeStyles((theme) => ({
        card:{
            maxWidth: '700px',
            marginLeft:'auto',
            marginRight: 'auto',
            marginTop: '50px',
            maxHeight: '700px',
            padding: '50px'
        },
        avatar:{
            width: 250 , 
            height: 250, 
            padding: '20px'
        },
        accountIcon:{
            fontSize: 250 
        }

    }));

    const [user, setUser] = useState("");


    useEffect(()=>{
        fetch('http://localhost:8080/users/current',{
            headers:{
              "Authorization": localStorage.getItem("token")
            }
          })
          .then(response => response.json())
          .then(data => {
              setUser(data);
          });
    },[])


    
    const classes = useStyles();
    return(
        <div>
        <Card raised className={classes.card}>
            <div>
            <Avatar className={classes.avatar}>
                <AccountCircle className={classes.accountIcon}/>
            </Avatar>
            </div>
            <div>
                <Typography variant="h2">

                    {user.name} {user.surname}
                </Typography>
            </div>
        </Card>

        </div>
    )

}