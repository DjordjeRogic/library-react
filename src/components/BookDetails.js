
import { useParams } from "react-router";
import React, { useEffect,useState } from "react";
import { useHistory } from "react-router-dom";
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import AuthorList from './AuthorList'
import '../App.css'
import '../css/BookDetails.css'
import { Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    mainGrid:{
        minHeight: '100vh',
        backgroundColor: '#E8E8E8',
        width:'100%'
      },
      image:{
          width:250,
          height:250
      },
      rentButton:{
          display:'block',
          margin:'auto',
          marginTop: '10px'
      }
}))

export default function BookDetails(){
    const params = useParams();
    const classes = useStyles();
    const [book,setBook] = useState("");
    const [authors,setAuthors] = useState([]);
    const [bookExists,setBookExists] = useState(true);
    const [numberOfCopies, setNumberOfCopies] = useState(0);

    let history = useHistory();

    function getNumberOfCopies() {

        fetch("http://localhost:8080/books/" + params.id + "/copies/available/size", {
            headers: {
                "Authorization": localStorage.getItem("token")
            }
        })
            .then(response => response.json())
            .then(data => setNumberOfCopies(data))
    }



    useEffect(()=>{

        if(isNaN(params.id)){
            history.push('/books');
        }

        if(localStorage.getItem("token") == null){
            history.push('/');
        }

        fetch('http://localhost:8080/books/'+params.id,{
            headers:{
              "Authorization": localStorage.getItem("token")
            }
          })
          .then(response => {
              
            if(response.ok){
                response.json().then(data=>{
                    setBook(data)
                    setAuthors(data.authors)
               });
            }else{
                setBookExists(false);                
            }
        
        })

        
        getNumberOfCopies()

    }, [])

    function rentABook(){
        fetch("http://localhost:8080/books/"+params.id+'/rent', {
            method: "PUT",
            mode: "cors",
            credentials: "same-origin",
            headers: {
                "Content-Type": "application/json",
                "Authorization": localStorage.getItem("token")
            },
        })
            .then(response => response.json())
            .then(data => {
                 getNumberOfCopies()
            })
    }
    
    return (
        <div>
            
        {bookExists && <Card  className="container-center card-book-details" raised>
                <div className="image-container">
                    <img className = {classes.image} src = {book.imageUrl}/>
                    <Typography variant="subtitle1s">Aviable copies: {numberOfCopies}</Typography>
                    <Button disabled={numberOfCopies===0} onClick={rentABook} color="primary" variant="contained" className={classes.rentButton} float="left">Rent!</Button>
                </div>
                <div className="book-details-container">
                    <Typography variant="h2">{book.name}</Typography>
                    <AuthorList authors={authors}/>
                    <Typography variant="subtitle1"><b>Description:</b></Typography>
                    <Typography variant="subtitle2">{book.description}</Typography>
                </div>
        </Card>}
        {!bookExists && 
            <Card className="container-center card-book-details">
                <Typography align="center" variant="h3">This book doesn't exist! </Typography>
            </Card>
        }
        </div>

    )
}