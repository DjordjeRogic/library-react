import { useHistory } from "react-router-dom";
import { GridListTile,GridListTileBar} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

export default function BookTile(props){

    let history = useHistory();

    const useStyles = makeStyles((theme) => ({
        gridTile:{
            height:300, 
            width: 200, 
            margin:'20px'
        },
        img:{
            maxHeight:300, 
            maxWidth: 300,
            cursor: 'pointer'
        }
    }))

    let classes = useStyles();

    function openBookDetailsPage(id){
        history.push('/books/'+id)
      }
    
    return(
        <GridListTile className={classes.gridTile} key={props.book.id}>
            <img onClick={()=>openBookDetailsPage(props.book.id)} className={classes.img} src={props.book.imageUrl} alt={props.book.name} />
            <GridListTileBar 
            title={props.book.name}
            />
        </GridListTile>
    )
}

BookTile.defaulProps ={
    books:[],
}

BookTile.propTypes = {
    books: PropTypes.array.isRequired,
}