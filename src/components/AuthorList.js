import { ListItem,ListItemAvatar,Avatar,List,ListItemText,IconButton } from '@material-ui/core';
import CreateIcon from '@material-ui/icons/Create';
import HighlightOffIcon from '@material-ui/icons/HighlightOff'
import PropTypes from 'prop-types';

export default function AuthorList(props){
    function returnAuthorName(author){       
        const{firstName, middleName, lastName} = author

        let name = firstName+" ";
        if(middleName != null){
            name += middleName+" ";
        }
        name += lastName;
        return name;
    }

    return (
        <List>
            {props.authors.map((author,index)=>{
            return(
            <ListItem key={index}>
                <ListItemAvatar  fontSize="small">
                    <Avatar>
                    <CreateIcon />
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary={returnAuthorName(author)}/>
                {props.listType ==="complexList" && <IconButton onClick={()=>props.removeSelectedAuthor(author)}>
                    <HighlightOffIcon fontSize="small" />
                </IconButton>}
            </ListItem>
            )
            })}
        </List>
    )
    

}

AuthorList.defaulProps ={
    authors:[],
    removeSelectedAuthor:null,
    listType:"simpleList"
    
}

AuthorList.propTypes = {

    authors: PropTypes.array.isRequired,
    removeSelectedAuthor: PropTypes.func,
    listType: PropTypes.string
}

