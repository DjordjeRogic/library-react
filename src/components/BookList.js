import React, { useEffect,useState } from "react";
import { useHistory } from "react-router-dom";
import GridList from '@material-ui/core/GridList';
import BookTile from './BookTile'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridTile:{
    cursor: 'pointer'
  },
  centerDiv:{
    margin:'100px',
  },
  bookListImage:{
    height:'300px', 
    width: '300px',
    cursor: 'pointer'
  },
  gridTile:{
    height:'350px',
    width: '250px'
  },
}));


export default function BookList(){
    let history = useHistory();
    const classes = useStyles();
    const [books,setBooks] = useState([]);
    function getBooks(){
        fetch('http://localhost:8080/books',{
          headers:{
            "Authorization": localStorage.getItem("token")
          }
        })
        .then(response => response.json())
        .then(data => {
            setBooks(data.content);
        });
    
    }

    useEffect(()=>{
        getBooks()
    },[])

    return(
        <div className={classes.centerDiv}>
        <GridList className={classes.gridList} spacing={50} >
        {books.map((book,index) => (
            <BookTile key={index} book={book}/>
        ))}
      </GridList>
        </div>
    );
}


