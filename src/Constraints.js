import books_img from './books.png';
import books_img_nav from './books_navbar.png';

export const DEFAULT_BOOKS_IMAGE = books_img
export const DEFAULT_BOOKS_IMAGE_NAVBAR = books_img_nav
export const ACCOUNT_MENU_ID = 'primary-search-account-menu';